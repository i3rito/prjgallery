<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitbfb4292f1d914870b27910cccee053f4
{
    public static $fallbackDirsPsr4 = array (
        0 => __DIR__ . '/../..' . '/src/php/lib',
    );

    public static $prefixesPsr0 = array (
        'P' => 
        array (
            'Parsedown' => 
            array (
                0 => __DIR__ . '/..' . '/erusev/parsedown',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->fallbackDirsPsr4 = ComposerStaticInitbfb4292f1d914870b27910cccee053f4::$fallbackDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInitbfb4292f1d914870b27910cccee053f4::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
